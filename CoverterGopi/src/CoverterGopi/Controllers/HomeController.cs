﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using CoverterGopi.Models;

namespace CoverterGopi.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewData["Title"] = "Converter App by Gopi";
            ViewData["Result"] = "";
            return View(new Converter());
        }

        public IActionResult Convert(Converter converter)
        {
            if (ModelState.IsValid)
            {
                ViewData["Title"] = "Converted by Gopi";
                ViewData["Result"] = "Temperature in C = "+ (int)((converter.Temperature_F-32)*5/9);
            }else
            {
                ViewData["Title"] = "Converter App by Gopi";
                ViewData["Result"] = "";
            }
            return View("Index", converter);
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
