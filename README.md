﻿# Readme

This exercise involves creating a simple, responsive, 1-page ASP.NET 5 MVC web application that accepts a ZIP code (to be used later) and a temperature in degrees F. It will convert the temperature to degrees C.   No persistence is required. 

## Design process

1.  Determine **controller** requirements - how many? What will you name them?  
2.  Determine **view** requirements - how many? What will you name them?
3.  Determine **model** requirements - how many?  What will you name them?

## Implementation

1.  Create a **repository**.
2.  Create an **MVC project**.
3.  Create your **model**.  Add annotations and validations to get the required behavior. 
4.  Create your **controller**.  Implement the default Index action. We don't need scaffolding, since we won't be doing CRUD.
5.  Create your **view** (an example is provided below). 
6.  Implement the **Convert** action (called when the user submits the form).
7.  Clean up unneeded navigation in _Layout.cs.

## behavior looks like

 ![A05_1.JPG](https://bitbucket.org/repo/gq9yXL/images/3448641976-A05_1.JPG)


![A05_2.JPG](https://bitbucket.org/repo/gq9yXL/images/1153467536-A05_2.JPG)


![A05_3.JPG](https://bitbucket.org/repo/gq9yXL/images/373637756-A05_3.JPG)



![A05_4.JPG](https://bitbucket.org/repo/gq9yXL/images/1320937490-A05_4.JPG)


![A05_5.JPG](https://bitbucket.org/repo/gq9yXL/images/992453923-A05_5.JPG)